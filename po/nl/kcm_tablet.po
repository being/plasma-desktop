# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2021, 2022, 2024 Freek de Kruijf <freekdekruijf@kde.nl>
# SPDX-FileCopyrightText: 2024 pieter <pieterkristensen@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-10 00:39+0000\n"
"PO-Revision-Date: 2024-04-26 11:05+0200\n"
"Last-Translator: pieter <pieterkristensen@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.2\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Primary (standaard)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Portret"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Landschap"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Omgekeerd portret"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Omgekeerd landschap"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Volg het actieve scherm"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "In uitvoer laten passen"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Uitvoer in tablet laten passen"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Aangepaste grootte"

#: ui/main.qml:28
#, kde-format
msgid "No drawing tablets found"
msgstr "Geen tekentabletten gevonden"

#: ui/main.qml:29
#, kde-format
msgid "Connect a drawing tablet"
msgstr "Met een tekentablet verbinden"

#: ui/main.qml:41
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Apparaat:"

#: ui/main.qml:79
#, kde-format
msgid "Target display:"
msgstr "Doel-beeldscherm:"

#: ui/main.qml:98
#, kde-format
msgid "Orientation:"
msgstr "Oriëntatie:"

#: ui/main.qml:110
#, kde-format
msgid "Left handed mode:"
msgstr "Modus linkshandig:"

#: ui/main.qml:120
#, kde-format
msgid "Area:"
msgstr "Gebied:"

#: ui/main.qml:208
#, kde-format
msgid "Resize the tablet area"
msgstr "Het tabletgebied van grootte wijzigen"

#: ui/main.qml:232
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Beeldverhouding vergrendelen"

#: ui/main.qml:240
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: ui/main.qml:249
#, kde-format
msgid "Pen button 1:"
msgstr "Penknop 1:"

#: ui/main.qml:250
#, kde-format
msgid "Pen button 2:"
msgstr "Penknop 2:"

#: ui/main.qml:251
#, kde-format
msgid "Pen button 3:"
msgstr "Penknop 3:"

#: ui/main.qml:295
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Blok:"

#: ui/main.qml:306
#, kde-format
msgid "None"
msgstr "Geen"

#: ui/main.qml:328
#, kde-format
msgid "Pad button %1:"
msgstr "Padknop %1:"

#~ msgid "Tool Button 1"
#~ msgstr "Hulpmiddelknop 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - 2021"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"

#~ msgid "Tablet"
#~ msgstr "Tablet"
